import dataclasses
from typing import Callable, Generic, List, Optional, TypeVar



def cached(f):
    cache = []

    def decor_cashed(*args):
        for (key, cached_value) in cache:
            if key == args:
                return cached_value

        res = f(*args)
        if len(cache) > 2:
            cache.pop(2)
        cache.insert(0,(args,res))
        return res

    return decor_cashed



T = TypeVar("T")

@dataclasses.dataclass
class ParseResult(Generic[T]):
    value: Optional[T]
    rest: str

    @staticmethod
    def invalid(rest: str) -> "ParseResult":
        return ParseResult(value=None, rest=rest)

    def is_valid(self) -> bool:
        return self.value is not None



Parser = Callable[[str], ParseResult[T]]

def parser_char(char: str) -> Parser[str]:
    if len(char) != 1:
        raise ValueError

    def remains(inp: str):
        if inp.startswith(char):
            return ParseResult(char, inp[1:])
        return ParseResult.invalid(inp)
    return remains



def parser_repeat(parser: Parser[T]) -> Parser[List[T]]:

    def remains(inp: str):
        values = []
        while True:
            tmp = parser(inp)
            if tmp.is_valid():
                values.append(tmp.value)
                inp = tmp.rest
            else:
                return ParseResult(values, tmp.rest)
    return remains



def parser_seq(parsers: List[Parser]) -> Parser:

    def remains(inp: str):
        values = []
        tmpInp = inp
        if len(parsers) == 0:
            return  ParseResult([], inp)
        tmp = Parser
        for prs in parsers:
            tmp = prs(tmpInp)
            if tmp.is_valid():
                values.append(tmp.value)
                tmpInp = tmp.rest
            else:
                return ParseResult.invalid(inp)
        return ParseResult(values, tmp.rest)
    return remains



def parser_choice(parsers: List[Parser]) -> Parser:

    def remains(inp: str):
        if len(parsers) == 0:
            return ParseResult.invalid(inp)
        for prs in parsers:
            tmp = prs(inp)
            if tmp.is_valid():
                return ParseResult(tmp.value,tmp.rest)
        return ParseResult.invalid(inp)
    return remains



R = TypeVar("R")

def parser_map(parser: Parser[R], map_fn: Callable[[R], Optional[T]]) -> Parser[T]:

    def remains(inp: str):
        tmp = parser(inp)
        if tmp.is_valid():
            tmpMap = map_fn(tmp.value)
            if tmpMap is None:
                return ParseResult.invalid(inp)
            return ParseResult(tmpMap, tmp.rest)
        return ParseResult.invalid(inp)
    return remains



def parser_matches(filter_fn: Callable[[str], bool]) -> Parser[str]:

    def remains(inp: str):
        if len(inp) == 0:
            return ParseResult.invalid(inp)
        if filter_fn(inp[0]):
            return parser_char(inp[0])(inp)
        return ParseResult.invalid(inp)
    return remains



def parser_string(string: str) -> Parser[str]:

    def remains(inp: str):
        if inp.startswith(string):
            return ParseResult(string, inp[len(string):])
        return ParseResult.invalid(inp)
    return remains



def parser_int() -> Parser[int]:

    def remains(inp: str):
        if len(inp) == 0:
            return ParseResult.invalid(inp)
        tmp = ""
        for i in inp:
            if i.isdigit():
                tmp += str(i)
            else:
                break
        if len(tmp) == 0:
            return ParseResult.invalid(inp)
        tmpStr = parser_string(tmp)(inp)
        if tmpStr.is_valid():
            return ParseResult(int(tmpStr.value), tmpStr.rest)
        return ParseResult.invalid(inp)
    return remains